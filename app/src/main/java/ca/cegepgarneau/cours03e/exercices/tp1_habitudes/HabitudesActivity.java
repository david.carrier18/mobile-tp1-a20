package ca.cegepgarneau.cours03e.exercices.tp1_habitudes;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class HabitudesActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView lbl_Utilisateur ,lbl_Habitude, lbl_Compteur;
    private Button btn_Incremente, btn_Precedent, btn_Suivant, btn_Partager;

    //Cette variable sera utile pour se déplacer dans la liste d'habitude lorsqu'on appuie sur les
    //boutons "<<" et ">>". La valeur de cette variable correspond à l'indice de l'habitude dans la
    //liste.
    private int habitudeCourante = 0;

    private ArrayList<Habitude> lst_Habitudes;

    SharedPreferences prefs;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habitudes);

        lbl_Utilisateur = findViewById(R.id.lbl_Utilisateur);
        lbl_Habitude = findViewById(R.id.lbl_Habitude);
        lbl_Compteur = findViewById(R.id.lbl_Compteur);

        btn_Incremente = findViewById(R.id.btn_Incremente);
        btn_Precedent = findViewById(R.id.btn_Precedent);
        btn_Suivant = findViewById(R.id.btn_Suivant);
        btn_Partager = findViewById(R.id.btn_Partager);


        prefs = getSharedPreferences("habitudes", MODE_PRIVATE);

        //Création de la liste d'habitudes et insertion de 3 habitudes.
        lst_Habitudes = new ArrayList<>();

        lst_Habitudes.add(new Habitude("Aller courir", prefs.getInt("habitude0", 0)));
        lst_Habitudes.add(new Habitude("Lire mon livre", prefs.getInt("habitude1", 0)));
        lst_Habitudes.add(new Habitude("Étudier", prefs.getInt("habitude2", 0)));


        //Titre contenant le nom de l'utilisateur en cours
        lbl_Utilisateur.setText(String.format("%s %s",this.getString(R.string.habitude), getIntent().getStringExtra("pseudo")));

        btn_Suivant.setOnClickListener(this);
        btn_Precedent.setOnClickListener(this);
        btn_Incremente.setOnClickListener(this);
        btn_Partager.setOnClickListener(this);

        lbl_Habitude.setText(lst_Habitudes.get(habitudeCourante).GetNom());
        lbl_Compteur.setText(Integer.toString(lst_Habitudes.get(habitudeCourante).GetCompteur()));

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_Incremente :
                //La valeur du compteur d'une habitude est stocké dans les préférences de l'application.
                lst_Habitudes.get(habitudeCourante).IncrementerHabitude();
                lbl_Compteur.setText(Integer.toString(lst_Habitudes.get(habitudeCourante).GetCompteur()));
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("habitude" + habitudeCourante,lst_Habitudes.get(habitudeCourante).GetCompteur());
                editor.apply();
                break;
            case R.id.btn_Precedent :
                //La variable "habitudeCourante" est ajustée en fonction de sa prochaine position
                //pour ne pas avoir un indice en dehord de la liste
                    if(!(habitudeCourante - 1 == -1)){
                        habitudeCourante--;
                    }else{
                        habitudeCourante = 2;
                    }
                lbl_Habitude.setText(lst_Habitudes.get(habitudeCourante).GetNom());
                lbl_Compteur.setText(Integer.toString(lst_Habitudes.get(habitudeCourante).GetCompteur()));
                break;
            case R.id.btn_Suivant :
                //La variable "habitudeCourante" est ajustée en fonction de sa prochaine position
                //pour ne pas avoir un indice en dehord de la liste
                if(!(habitudeCourante + 1 == 3)){
                    habitudeCourante++;
                }else{
                    habitudeCourante = 0;
                }
                lbl_Habitude.setText(lst_Habitudes.get(habitudeCourante).GetNom());
                lbl_Compteur.setText(Integer.toString(lst_Habitudes.get(habitudeCourante).GetCompteur()));
                break;
            case R.id.btn_Partager :
                String message = String.format("%s : %s fois",lst_Habitudes.get(habitudeCourante).GetNom(), lst_Habitudes.get(habitudeCourante).GetCompteur());
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(intent);
                break;
        }
    }


    //Permet d'avoir un bouton menant aux réglages de l'application.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.habitude, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reglage) {
            Intent intent = new Intent(this, ConfigurationActivity.class);
            intent.putExtra("pseudo", getIntent().getStringExtra("pseudo"));
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}