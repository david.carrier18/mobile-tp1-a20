package ca.cegepgarneau.cours03e.exercices.tp1_habitudes;

import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txt_nouveauPseudo;
    private Button btn_reinitialiser;

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        //On fait apparaitre la flèche de retour du menu.
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        prefs = getSharedPreferences("habitudes", MODE_PRIVATE);

        txt_nouveauPseudo = findViewById(R.id.txt_NouveauPseudo);
        btn_reinitialiser = findViewById(R.id.btn_Reinitialiser);

        txt_nouveauPseudo.setText(getIntent().getStringExtra("pseudo"));
        btn_reinitialiser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_Reinitialiser:
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Toast.makeText(ConfigurationActivity.this, this.getString(R.string.msg_DonneeEffacee), Toast.LENGTH_LONG).show();
                break;
        }
    }

    //Lorsque la flèche de retour su menu est pressé, on appele la même fonction qui si on fesait un
    //retour en arrière avec le menu d'android.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HabitudesActivity.class);
        intent.putExtra("pseudo", txt_nouveauPseudo.getText().toString());
        startActivity(intent);
    }

    //Fonction utile qui permet d'enlever le focus d'un champ lorsqu'on tape à l'extérieur de celui-ci
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}