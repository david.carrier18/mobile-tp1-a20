package ca.cegepgarneau.cours03e.exercices.tp1_habitudes;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_Connexion;
    private EditText txt_Pseudo;
    private EditText txt_Mdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Supprime le bouton retour sur l'écran de connexion
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        btn_Connexion = findViewById(R.id.btn_Connexion);

        btn_Connexion.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_Connexion:
                txt_Pseudo = findViewById(R.id.txt_Pseudo);
                txt_Mdp = findViewById(R.id.txt_Mdp);
                //Vérification des données de connexion
                if(txt_Pseudo.getText().toString().equals("bot") && txt_Mdp.getText().toString().equals("123")){
                    Intent intent = new Intent(this, HabitudesActivity.class);
                    intent.putExtra("pseudo", txt_Pseudo.getText().toString());
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this,  this.getString(R.string.msg_ErrConnexion), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    //Fonction utile qui permet d'enlever le focus d'un champ lorsqu'on tape à l'extérieur de celui-ci
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}