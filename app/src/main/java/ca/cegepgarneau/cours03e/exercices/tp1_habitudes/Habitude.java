package ca.cegepgarneau.cours03e.exercices.tp1_habitudes;

//Classe représentant une habitude
public class Habitude {

    private String str_nomHabitude;
    private int i_compteur;

    public Habitude(String nom, int cpt){
        this.str_nomHabitude = nom;
        this.i_compteur = cpt;
    }

    public String GetNom(){
        return str_nomHabitude;
    }

    public int GetCompteur(){
        return i_compteur;
    }

    public void IncrementerHabitude(){ this.i_compteur+=1; }
}
